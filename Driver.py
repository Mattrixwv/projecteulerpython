#ProjectEulerPython/Driver.py
#Matthew Ellison
# Created: 07-19-20
#Modified: 07-19-20
#This is the driver function for the Java version of the ProjectEuler project
"""
Copyright (C) 2020  Matthew Ellison

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Lesser General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""


from Benchmark import Benchmark
from ProblemSelection import ProblemSelection


class Driver:
	#A class to hold the possible menu selections
	class SELECTION:
		SOLVE = 1
		DESCRIPTION = 2
		LIST = 3
		BENCHMARK = 4
		EXIT = 5
		SIZE = 6

	#Drives the program
	@staticmethod
	def main():
		selection = Driver.SELECTION.SIZE	#Holds the menu selection of the user
		while(selection != Driver.SELECTION.EXIT):
			#Print the menu and prompt the user to select an action
			Driver.printMenu()
			selection = Driver.getMenuSelection()

			if(selection == Driver.SELECTION.SOLVE):
				Driver.solveMenu()
			elif(selection == Driver.SELECTION.DESCRIPTION):
				Driver.descriptionMenu()
			elif(selection == Driver.SELECTION.LIST):
				ProblemSelection.listProblems()
			elif(selection == Driver.SELECTION.BENCHMARK):
				Benchmark.benchmarkMenu()
			elif(selection == Driver.SELECTION.EXIT):
				selection = selection
			else:
				Driver.printErrorMessage()

	#Print the menu
	@staticmethod
	def printMenu():
		print("1. Solve a problem")
		print("2. Print a problem description")
		print("3. List valid problem numbers")
		print("4. Benchmark")
		print("5. Exit")
		print()

	#Get a menu selection from the user
	@staticmethod
	def getMenuSelection() -> int:
		selection = int(input(""))
		while(not Driver.isValidMenu(selection)):
			print("That is an invalid option!\nPress Enter to continue")
			Driver.printMenu()
			selection = int(input(""))
		return selection

	#Make sure the value passed in is a valid menu option
	@staticmethod
	def isValidMenu(selection: int) -> bool:
		if((selection > 0) and (selection < (Driver.SELECTION.SIZE))):
			return True
		else:
			return False

	#Print an error message
	@staticmethod
	def printErrorMessage():
		print("That is an invalid selection!")

	#Handle what happens when a user wants to solve a problem
	@staticmethod
	def solveMenu():
		problemNumber = ProblemSelection.getProblemNumber()
		#This selection solves all problems in order
		if(problemNumber == 0):
			#Solve to every valid problem number, skipping over 0
			for problemLocation in range(1, len(ProblemSelection.problemNumbers)):
				#Solve the problem
				print("\n\n" + str(ProblemSelection.problemNumbers[problemLocation]) + ". ")
				ProblemSelection.solveProblem(ProblemSelection.problemNumbers[problemLocation])
				print("\n\n")
		#This is if a single problem number was chosen
		else:
			#Solve the problem
			ProblemSelection.solveProblem(problemNumber)

	#Handle what happens when a user wants to see the description of a problem
	@staticmethod
	def descriptionMenu():
		#Give some extra space to print the description
		print('\n')

		#Get the problem number
		problemNumber = ProblemSelection.getProblemNumber()

		#If the problem number is 0 print out all the description
		if(problemNumber == 0):
			#Print description for every valid problem number
			for problemLocation in range(1, len(ProblemSelection.problemNumbers)):
				#Print the problem's description
				print(str(ProblemSelection.problemNumbers[problemLocation]) + ". ")
				ProblemSelection.printDescription(ProblemSelection.problemNumbers[problemLocation])
				print()
		#Otherwise print out a single problem's description
		else:
			ProblemSelection.printDescription(problemNumber)


if __name__ == "__main__":
	Driver.main()
