#ProjectEuler/Python/Problem31.py
#Matthew Ellison
# Created: 06-19-20
#Modified: 07-24-21
#How many different ways can £2 be made using any number of coins?
#Unless otherwise listed, all of my non-standard imports can be gotten from my pyClasses repository at https://bitbucket.org/Mattrixwv/pyClasses
"""
	Copyright (C) 2021  Matthew Ellison

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Lesser General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""


from Problems.Problem import Problem


class Problem31(Problem):
	#Variables
	__desiredValue = 200	#The value of coins we want

	#Functions
	#Constructor
	def __init__(self) -> None:
		super().__init__("How many different ways can 2 pounds be made using any number of coins?")
		self.permutations = 0

	#Operational functions
	#Solve the problem
	def solve(self) -> None:
		#If the problem has already been solved do nothing and end the function
		if(self.solved):
			return

		#Start the timer
		self.timer.start()


		#Start with 200p and remove the necessary coins with each loop
		for pound2 in range(self.__desiredValue, -1, -200):
			for pound1 in range(pound2, -1, -100):
				for pence50 in range(pound1, -1, -50):
					for pence20 in range(pence50, -1, -20):
						for pence10 in range(pence20, -1, -10):
							for pence5 in range(pence10, -1, -5):
								for pence2 in range(pence5, -1, -2):
									self.permutations += 1


		#Stop the timer
		self.timer.stop()

		#Throw a flag to show the problem is solved
		self.solved = True

	#Reset the problem so it can be run again
	def reset(self) -> None:
		super().reset()
		self.permutations = 0

	#Gets
	#Returns the result of solving the problem
	def getResult(self) -> str:
		self.solvedCheck("result")
		return f"There are {self.permutations} ways to make 2 pounds with the given denominations of coins"
	#Returns the number of correct permutations of the coins
	def getPermutations(self) -> int:
		self.solvedCheck("number of correct permutations of the coins")
		return self.permutations


""" Results:
There are 73682 ways to make 2 pounds with the given denominations of coins
It took an average of 5.519 milliseconds to run this problem through 100 iterations
"""
