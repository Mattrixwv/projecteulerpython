#ProjectEuler/ProjectEulerPython/Problems/Problem35.py
#Matthew Ellison
# Created: 06-05-21
#Modified: 06-05-21
#Find the sum of all numbers which are equal to the sum of the factorial of their digits
#Unless otherwise listed, all of my non-standard imports can be gotten from my pyClasses repository at https://bitbucket.org/Mattrixwv/pyClasses
"""
	Copyright (C) 2021  Matthew Ellison

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Lesser General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""


from Problems.Problem import Problem
import NumberAlgorithms


class Problem35(Problem):
	#Variables
	__max_num = 999999

	#Functions
	#Constructor
	def __init__(self) -> None:
		super().__init__("Find the sum of all numbers which are equal to the sum of the factorial of their digits")
		self.primes = []
		self.circularPrimes = []

	#Returns a list of all rotations of a string passed to it
	def getRotations(self, str: str) -> list:
		rotations = []
		rotations.append(str)
		for _ in range(1, len(str)):
			str = str[1::] + str[0]
			rotations.append(str)
		return rotations

	#Operational functions
	#Solve the problem
	def solve(self) -> None:
		#If the problem has already been solved do nothing and end the function
		if(self.solved):
			return

		#Start the timer
		self.timer.start()


		#Get all primes under 1,000,000
		self.primes = NumberAlgorithms.getPrimes(self.__max_num)
		#Go through all primes, get all their rotations, and check if those numbers are also primes
		for prime in self.primes:
			allRotationsPrime = True
			#Get all of the rotations of the prime and see if they are also prime
			rotations = self.getRotations(str(prime))
			for rotation in rotations:
				p = int(rotation)
				if(p not in self.primes):
					allRotationsPrime = False
					break
			#If all rotations are prime add it to the list of circular primes
			if(allRotationsPrime):
				self.circularPrimes.append(prime)


		#Stop the timer
		self.timer.stop()

		#Throw a flag to show the problem is solved
		self.solved = True

	#Reset the problem so it can be run again
	def reset(self) -> None:
		super().reset()
		self.primes = []
		self.circularPrimes = []

	#Gets
	#Returns a string with the solution to the problem
	def getResult(self) -> str:
		self.solvedCheck("result")
		return f"The number of all circular prime numbers under {self.__max_num} is {len(self.circularPrimes)}"
	#Returns the list of primes < max_num
	def getPrimes(self) -> list:
		self.solvedCheck("list of primes")
		return self.primes
	#Returns the list of circular primes < max_num
	def getCircularPrimes(self) -> list:
		self.solvedCheck("list of circular primes")
		return self.circularPrimes
	#Returns the number of circular primes
	def getNumCircularPrimes(self) -> list:
		self.solvedCheck("number of circular primes")
		return len(self.circularPrimes)


""" Results:
The number of all circular prime numbers under 999999 is 55
It took 106.369 seconds to solve this algorithm
"""
