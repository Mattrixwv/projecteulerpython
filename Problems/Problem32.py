#ProjectEuler/ProjectEulerPython/Problems/Problem32.py
#Matthew Ellison
# Created: 07-28-20
#Modified: 07-24-21
#Find the sum of all products whose multiplicand/multiplier/product identity can be written as a 1 through 9 pandigital.
#Unless otherwise listed, all of my non-standard imports can be gotten from my pyClasses repository at https://bitbucket.org/Mattrixwv/pyClasses
"""
	Copyright (C) 2021  Matthew Ellison

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Lesser General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""


from Problems.Problem import Problem


class Problem32(Problem):
	#Structures
	class ProductSet:
		def __init__(self, multiplicand: int, multiplier: int) -> None:
			self.multiplicand = multiplicand
			self.multiplier = multiplier
		def getProduct(self) -> int:
			return (self.multiplicand * self.multiplier)
		def getNumString(self) -> str:
			return (str(self.multiplicand) + str(self.multiplier) + str(self.getProduct()))
		def __str__(self) -> str:
			return (str(self.multiplicand) + " x " + str(self.multiplier) + " = " + str(self.getProduct()))
		def __repr__(self) -> str:
			return self.__str__()
		def __eq__(self, secondSet) -> bool:
			return (self.getProduct() == secondSet.getProduct())

	#Variables
	#Static variables
	__topMultiplicand = 99	#The largest multiplicand to check
	__topMultiplier = 4999	#The largest multiplier to check

	#Functions
	#Constructor
	def __init__(self) -> None:
		super().__init__("Find the sum of all products whose multiplicand/multiplier/product identity can be written as a 1 through 9 pandigital.")
		self.listOfProducts = []	#The list of unique products that are 1-9 pandigital
		self.sumOfPandigitals = 0	#The sum of the products of the pandigital numbers

	#Operational functions
	#Solve the problem
	def solve(self) -> None:
		#If the problem has already been solved do nothing and end the function
		if(self.solved):
			return

		#Start the timer
		self.timer.start()


		#Create the multiplicand and start working your way up
		for multiplicand in range(1, self.__topMultiplicand + 1):
			#Run through all possible multipliers
			for multiplier in range(multiplicand, self.__topMultiplier + 1):
				currentProductSet = self.ProductSet(multiplicand, multiplier)
				#If the product is too long move on to the next possible number
				if(len(currentProductSet.getNumString()) > 9):
					break
				#If the current number is a pandigital that doesn't already exist in the list add it
				if(self.isPandigital(currentProductSet)):
					if(not currentProductSet in self.listOfProducts):
						self.listOfProducts.append(currentProductSet)

		#Get the sum of the products of the pandigitals
		for prod in self.listOfProducts:
			self.sumOfPandigitals += prod.getProduct()


		#Stop the timer
		self.timer.stop()

		#Throw a flag to show the problem is solved
		self.solved = True

	#Returns true if the passed productset is 1-9 pandigital
	def isPandigital(self, currentSet: ProductSet) -> bool:
		#Get the number out of the object and put them into a string
		numberString = currentSet.getNumString()
		#Make sure the string is the correct length
		if(len(numberString) != 9):
			return False
		#Make sure there is exactly one of this number contained in the string
		for panNumber in range(1, 10):
			#Make sure there is exactly one of this number contained in the string
			if(numberString.count(str(panNumber)) != 1):
				return False
		#If all numbers were found in the string return true
		return True

	#Reset the problem so it can be run again
	def reset(self) -> None:
		super().reset()
		self.listOfProducts.clear()
		self.sumOfPandigitals = 0

	#Gets
	#Returns the result of solving the problem
	def getResult(self) -> str:
		self.solvedCheck("result")
		return f"There are {self.listOfProducts} unique 1-9 pandigitals\nThe sum of the products of these pandigitals is {self.sumOfPandigitals}"
	#Returns the sum of the pandigitals
	def getSumOfPandigitals(self) -> int:
		self.solvedCheck("sum of the pandigitals")
		return self.sumOfPandigitals


""" Results:
There are 7 unique 1-9 pandigitals
The sum of the products of these pandigitals is 45228
It took an average of 130.157 milliseconds to run this problem through 100 iterations
"""
