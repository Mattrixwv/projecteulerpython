#ProjectEuler/Python/Problem27.py
#Matthew Ellison
# Created: 09-15-19
#Modified: 07-24-21
#Find the product of the coefficients, |a| < 1000 and |b| <= 1000, for the quadratic expression that produces the maximum number of primes for consecutive values of n, starting with n=0.
#Unless otherwise listed, all of my non-standard imports can be gotten from my pyClasses repository at https://bitbucket.org/Mattrixwv/pyClasses
"""
	Copyright (C) 2020  Matthew Ellison

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Lesser General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""


from Problems.Problem import Problem
import NumberAlgorithms


class Problem27(Problem):
	#Functions
	#Constructor
	def __init__(self) -> None:
		super().__init__("Find the product of the coefficients, |a| < 1000 and |b| <= 1000, for the quadratic expression that produces the maximum number of primes for consecutive values of n, starting with n=0")
		self.topA = 0	#The A for the most n's generated
		self.topB = 0	#The B for the most n's generated
		self.topN = 0	#The most n's generated
		self.primes = []	#A list of all primes that could possibly be generated with this formula

	#Operational functions
	#Solve the problem
	def solve(self) -> None:
		#If the problem has already been solved do nothing and end the function
		if(self.solved):
			return

		#Start the timer
		self.timer.start()


		#Get the primes
		primes = NumberAlgorithms.getPrimes(12000)	#A list of all primes that could possibly be generated with this formula
		#Start with the lowest possible A and check all possibilities after that
		for a in range(-999, 999):
			#Start with the lowest possible B and check all possibilities after that
			for b in range(-1000, 1000):
				#Start with n=0 and check the formula to see how many primes you can get get with concecutive n's
				n = 0
				quadratic = (n * n) + (a * n) + b
				while(quadratic in primes):
					n += 1
					quadratic = (n * n) + (a * n) + b
				n -= 1  #Negate an n because the last formula failed

				#Set all the largest numbers if this created more primes than any other
				if(n > self.topN):
					self.topN = n
					self.topB = b
					self.topA = a


		#Stop the timer
		self.timer.stop()

		#Save the results
		self.result = "The greatest number of primes found is " + str(self.topN)
		self.result += "\nIt was found with A = " + str(self.topA) + ", B = " + str(self.topB)
		self.result += "\nThe product of A and B is " + str(self.topA * self.topB)

		#Throw a flag to show the problem is solved
		self.solved = True

	#Reset the problem so it can be run again
	def reset(self) -> None:
		super().reset()
		self.topA = 0
		self.topB = 0
		self.topN = 0
		self.primes.clear()

	#Gets
	#Returns the result of solving the problem
	def getResult(self) -> str:
		self.solvedCheck("result")
		return f"The greatest number of primes found is {self.topN}\n" \
			f"It was found with A = {self.topA}, B = {self.topB}\n" \
			f"The product of A and B is {self.topA * self.topB}"
	#Returns the top A that was generated
	def getTopA(self) -> int:
		self.solvedCheck("largest A")
		return self.topA

	#Returns the top B that was generated
	def getTopB(self) -> int:
		self.solvedCheck("largest B")
		return self.topA

	#Returns the top N that was generated
	def getTopN(self) -> int:
		self.solvedCheck("largest N")
		return self.topA

	#Returns the product of A and B for the answer
	def getProduct(self) -> int:
		self.solvedCheck("product of A and B")
		return self.topA * self.topB


""" Results:
The greatest number of primes found is 70
It was found with A = -61, B = 971
The product of A and B is -59231
It took an average of 49.963 seconds to run this problem through 100 iterations
"""
