#Project Euler/Python/Problem10.py
#Matthew Ellison
# Created: 01-30-19
#Modified: 07-24-21
#Find the sum of all the primes below two million
#Unless otherwise listed, all of my non-standard imports can be gotten from my pyClasses repository at https://bitbucket.org/Mattrixwv/pyClasses
"""
	Copyright (C) 2021  Matthew Ellison

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Lesser General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""


from Problems.Problem import Problem
import NumberAlgorithms


class Problem10(Problem):
	#Variables
	__numberGreaterThanPrimes = 2000000 - 1		#Get all primes < this number

	#Functions
	#Constructor
	def __init__(self) -> None:
		super().__init__("Find the sum of all the primes below two million")
		self.sum = 0	#The sum of all of the prime numbers

	#Operational functions
	#Solve the function
	def solve(self) -> None:
		#If the problem has already been solved do nothing and end the function
		if(self.solved):
			return

		#Start the timer
		self.timer.start()


		#Get all of the primes < 2000000
		primes = NumberAlgorithms.getPrimes(self.__numberGreaterThanPrimes)
		#Get the sum of the list
		self.sum = sum(primes)


		#Stop the timer
		self.timer.stop()

		#Throw a flag to show the problem is solved
		self.solved = True

	#Reset the problem so it can be run again
	def reset(self) -> None:
		super().reset()
		self.sum = 0

	#Gets
	#Returns the result of solving the problem
	def getResult(self) -> str:
		self.solvedCheck("result")
		return f"The sum of all the prime numbers less than {self.__numberGreaterThanPrimes + 1} is {self.sum}"
	#Returns the sum that was requested
	def getSum(self) -> int:
		self.solvedCheck("sum")
		return self.sum


"""Results:
The sum of all the prime numbers less than 2000000 is 142913828922
It took an average of 7.187 seconds to run this problem through 100 iterations
"""
