#ProjectEuler/ProjectEulerPython/Problems/Problem.py
#Matthew Ellison
# Created: 07-11-20
#Modified: 07-23-21
#This is a base class for problems to use as a template
"""
	Copyright (C) 2021  Matthew Ellison

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Lesser General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""


import abc
from Stopwatch import Stopwatch
from Unsolved import Unsolved


class Problem(metaclass=abc.ABCMeta):
	#Functions
	#Make sure the problem has been solved and throw an exception if not
	def solvedCheck(self, str: str) -> None:
		if(not self.solved):
			raise Unsolved("You must solve the problem before you can see the " + str)
	#Constructor
	@abc.abstractmethod
	def __init__(self, description: str) -> None:
		#Instance variables
		self.timer = Stopwatch()
		self.description = description
		self.solved = False
	#Gets
	#Returns the description of the problem
	def getDescription(self) -> str:
		return self.description
	#Returns the time taken to run the problem as a string
	def getTime(self) -> str:
		self.solvedCheck("time it took to run the algorithm")
		return self.timer.getString()
	#Returns the timer as a stopwatch
	def getTimer(self) -> Stopwatch:
		self.solvedCheck("timer")
		return self.timer
	#Reset the problem so it can be run again
	def reset(self) -> None:
		self.timer.reset()
		self.solved = False
		self.result = ""
	#Solve the problem
	@abc.abstractmethod
	def solve(self) -> None:
		pass
	#Returns the result of solving the problem
	@abc.abstractmethod
	def getResult(self) -> str:
		pass
