#ProjectEuler/ProjectEulerPython/Problems/Problem38.py
#Matthew Ellison
# Created: 10-20-21
#Modified: 11-11-21
#What is the largest 1-9 pandigital number that can be formed as the concatenated product of an integer with 1, 2, ... n where n > 1
#Unless otherwise listed, all of my non-standard imports can be gotten from my pyClasses repository at https://bitbucket.org/Mattrixwv/pyClasses
"""
	Copyright (C) 2021  Matthew Ellison

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Lesser General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""


from Problems.Problem import Problem
import StringAlgorithms


class Problem38(Problem):
	#Variables
	__highest_possible_number = 9999	#The highest number that needs to be checked for a 1-9 pandigital

	#Functions
	#Constructor
	def __init__(self) -> None:
		super().__init__("What is the largest 1-9 pandigital number that can be formed as the concatenated product of an integer with 1, 2, ... n where n > 1")
		self.largestNum = 0	#The number passed to the executeFormula function that returns the largest pandigital
		self.pandigital = 0	#The largest pandigital number found

	#Operational functions
	#Take the number and add its multiples to a string to return
	def executeFormula(self, num: int) -> str:
		#Turn the current number into a string
		numStr = str(num)
		numStr += str(num * 2)
		#Multiply the number and append the product to the string until you have one long enough
		cnt = 3
		while(len(numStr) < 9):
			numStr += str(num * cnt)
			cnt += 1
		return numStr
	#Solve the problem
	def solve(self) -> None:
		#If the problem has already been solved do nothing and end the function
		if(self.solved):
			return

		#Start the timer
		self.timer.start()


		#Loop from 1 -> __highest_possible_num checking for pandigitals
		for cnt in range(1, self.__highest_possible_number + 1):
			#Get the string from the formula
			numStr = self.executeFormula(cnt)
			panNum = int(numStr)
			#If the number is pandigital save it as the highest number
			if(StringAlgorithms.isPandigital(numStr) and (panNum > self.pandigital)):
				self.largestNum = cnt
				self.pandigital = panNum


		#Stop the timer
		self.timer.stop()

		#Throw a flag to show the problem is solved
		self.solved = True

	#Reset the problem so it can be run again
	def reset(self) -> None:
		super().reset()

	#Gets
	#Returns a string with the solutino to the problem
	def getResult(self) -> str:
		self.solvedCheck("result")
		return f"The largest appended product pandigital is {self.pandigital}"
	#Returns the largest number
	def getLargestNum(self) -> int:
		self.solvedCheck("largest number")
		return self.largestNum
	#Returns the pandigital of the number
	def getPandigital(self) -> int:
		self.solvedCheck("pandigital")
		return self.pandigital


""" Results:
The largest appended product pandigital is 932718654
It took an average of 9.886 milliseconds to run this problem through 100 iterations
"""
