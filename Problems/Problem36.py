#ProjectEuler/ProjectEulerPython/Problems/Problem36.py
#Matthew Ellison
# Created: 06-29-21
#Modified: 07-24-21
#Find the sum of all numbers, less than one million, which are palindromic in base 10 and base 2.
#Unless otherwise listed, all of my non-standard imports can be gotten from my pyClasses repository at https://bitbucket.org/Mattrixwv/pyClasses
"""
	Copyright (C) 2021  Matthew Ellison

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Lesser General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""


from Problems.Problem import Problem
import NumberAlgorithms
import StringAlgorithms


class Problem36(Problem):
	#Variables
	__max_num = 999999	#The largest number that will be checked

	#Functions
	#Constructor
	def __init__(self) -> None:
		super().__init__("Find the sum of all numbers, less than one million, which are palindromic in base 10 and base 2.")
		self.palindromes = []
		self.sumOfPal = 0

	#Operational functions
	#Solve the problem
	def solve(self) -> None:
		#If the problem has already been solved do nothing and end the function
		if(self.solved):
			return

		#Start the timer
		self.timer.start()


		#Start with 1, check if it is a palindrome in base 10 and 2, and continue to __max_num
		for num in range(1, self.__max_num + 1):
			#Check if num is a palindrome
			if(StringAlgorithms.isPalindrome(str(num))):
				#Convert num to base 2 and see if that is a palindrome
				binNum = NumberAlgorithms.toBin(num)
				if(StringAlgorithms.isPalindrome(binNum)):
					#Add num to the list of palindromes
					self.palindromes.append(num)
		#Get the sum of all palindromes in the list
		self.sumOfPal = sum(self.palindromes)


		#Stop the timer
		self.timer.stop()

		#Throw a flag to show the problem is solved
		self.solved = True

	#Reset the problem so it can be run again
	def reset(self) -> None:
		super().reset()
		self.palindromes = []
		self.sum = 0

	#Gets
	#Returns a string with the solution to the problem
	def getResult(self) -> str:
		self.solvedCheck("result")
		return f"The sum of all base 10 and base 2 palindromic numbers < {self.__max_num} is {self.sumOfPal}"
	#Return the list of palindromes < MAX_NUM
	def getPalindromes(self) -> list:
		self.solvedCheck("list of palindromes")
		return self.palindromes
	#Return the sum of all elements in the list of palindromes
	def getSumOfPalindromes(self) -> int:
		self.solvedCheck("sum of all palindromes")
		return self.sumOfPal


""" Results:
The sum of all base 10 and base 2 palindromic numbers < 999999 is 872187
It took an average of 295.861 milliseconds to run this problem through 100 iterations
"""
