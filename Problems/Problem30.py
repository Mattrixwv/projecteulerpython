#ProjectEuler/Python/Problem30.py
#Matthew Ellison
# Created: 10-28-19
#Modified: 07-24-21
#Find the sum of all the numbers that can be written as the sum of the fifth powers of their digits.
#Unless otherwise listed, all of my non-standard imports can be gotten from my pyClasses repository at https://bitbucket.org/Mattrixwv/pyClasses
"""
	Copyright (C) 2021  Matthew Ellison

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Lesser General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""


from Problems.Problem import Problem


class Problem30(Problem):
	#Setup the variables
	__topNum = 1000000	#This is the largest number that will be checked
	__bottomNum = 2	#Starts with 2 because 0 and 1 don't count
	__powerRaised = 5	#This is the power that the digits are raised to

	#Functions
	#Constructor
	def __init__(self) -> None:
		super().__init__("Find the sum of all the numbers that can be written as the sum of the fifth powers of their digits.")
		self.sumOfFifthNumbers = []	#This is an ArrayList of the numbers that are the sum of the fifth power of their digits
		self.sum = 0	#This is the sum of the sumOfFifthNumbers list

	#Operational function
	#Solve the problem
	def solve(self) -> None:
		#If the problem has already been solved do nothing and end the function
		if(self.solved):
			return

		#Start the timer
		self.timer.start()


		#Start with the lowest number and increment until you reach the largest number
		for currentNum in range(self.__bottomNum, self.__topNum):
			#Get the digits of the number
			digits = self.getDigits(currentNum)
			#Get the sum of the powers
			sumOfPowers = 0
			for cnt in range(0, len(digits)):
				sumOfPowers += digits[cnt]**self.__powerRaised
			#Check if the sum of the powers is the same as the number
			#If it is add it to the list, otherwise continue to the next number
			if(sumOfPowers == currentNum):
				self.sumOfFifthNumbers.append(currentNum)


		#Stop the timer
		self.timer.stop()

		#Throw a flag to show the problem is solved
		self.solved = True

	#Returns a list with the individual digits of the number passed to it
	def getDigits(self, num: int) -> list:
		listOfDigits = []	#This list holds the individual digits of num
		#The easiest way to get the individual digits of a number is by converting it to a string
		digits = str(num)
		#Start with the first digit, convert it to an integer, store it in the list, and move to the next digit
		for cnt in range(0, len(digits)):
			listOfDigits.append(int(digits[cnt]))
		#Return the list of digits
		return listOfDigits

	#Reset the problem so it can be run again
	def reset(self) -> None:
		super().reset()
		self.sumOfFifthNumbers.clear()

	#Gets
	#Returns the result of solving the problem
	def getResult(self) -> str:
		self.solvedCheck("result")
		return f"The sum of all the numbers that can be written as the sum of the fifth powers of their digits is {sum(self.sumOfFifthNumbers)}"
	#Returns the top number to be checked
	def getTopNum(self) -> int:
		self.solvedCheck("largest number checked")
		return self.__topNum
	#Returns a copy of the vector holding all the number that are the sum of the fifth powers of their digits
	def getListOfSumsOfFifths(self) -> list:
		self.solvedCheck("list of all numbers that are the sum of the 5th power of their digits")
		return self.sumOfFifthNumbers
	#Returns the sum of all entries in sumOfFifthNumbers
	def getSumOfList(self) -> int:
		self.solvedCheck("sum of all numbers that are the sum of the 5th power of their digits")
		return sum(self.sumOfFifthNumbers)


""" Results:
The sum of all the numbers that can be written as the sum of the fifth powers of their digits is 443839
It took an average of 4.068 seconds to run this problem through 100 iterations
"""
