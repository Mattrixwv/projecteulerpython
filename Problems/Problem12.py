#ProjectEuler/Python/Problem12.py
#Matthew Ellison
# Created: 01-31-19
#Modified: 07-24-21
#What is the value of the first triangle number to have over five hundred divisors?
#Unless otherwise listed, all of my non-standard imports can be gotten from my pyClasses repository at https://bitbucket.org/Mattrixwv/pyClasses
"""
	Copyright (C) 2021  Matthew Ellison

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Lesser General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""


from Problems.Problem import Problem
import NumberAlgorithms


class Problem12(Problem):
	#Variables
	__goalDivisors = 500	#The number of divisors a number needs to reach

	#Functions
	#Constructor
	def __init__(self) -> None:
		super().__init__("What is the value of the first triangle number to have over five hundred divisors?")
		self.sum = 1
		self.counter = 2
		self.divisors = []

	def solve(self) -> None:
		#If the problem has already been solved do nothing and end the function
		if(self.solved):
			return

		#Start the timer
		self.timer.start()


		#Start at the first triangular number and loop around, moving to the next, until you find one with the appropriate number of divisors
		foundNumber = False		#A flag for when the triangular number has over __goalDivisors divisors
		while((not foundNumber) and (self.sum > 0)):	#Make sure you haven't caused an overflow and made trianularNumber negative
			#See how many divisors this triangular number has
			self.divisors = NumberAlgorithms.getDivisors(self.sum)
			#If it did have enough raise a flag to stop the loop
			if(len(self.divisors) > self.__goalDivisors):
				foundNumber = True
			else:
				self.sum += self.counter	#Add the next number to continue the triangular sequence
				self.counter += 1	#Advance to the next number in the triangular sequence


		#Stop the timer
		self.timer.stop()

		#Throw a flag to show the problem is solved
		self.solved = True

	#Reset the problem so it can be run again
	def reset(self) -> None:
		super().reset()
		self.sum = 1
		self.counter = 2
		self.divisors.clear()

	#Gets
	#Returns the result of solving the problem
	def getResult(self) -> str:
		self.solvedCheck("result")
		return f"The first triangular number with more than {self.__goalDivisors} divisors is {self.sum}"
	#Returns the triangular number
	def getTriangularNumber(self) -> int:
		self.solvedCheck("triangular number")
		return self.sum
	#Gets the final number that was added to the triangular number
	def getLastNumberAdded(self) -> int:
		self.solvedCheck("last number added to get the triangular number")
		return self.counter - 1
	#Returns the list of divisors of the requested number
	def getDivisorsOfTriangularNumber(self) -> list:
		self.solvedCheck("divisors of the triangular number")
		return self.divisors
	#Returns the number of divisors of the requested number
	def getNumberOfDivisors(self) -> int:
		self.solvedCheck("number of divisors of the triangular number")
		return len(self.divisors)


"""Results:
The first triangular number with more than 500 divisors is 76576500
It took an average of 5.932 seconds to run this problem through 100 iterations
"""
