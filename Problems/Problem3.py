#ProjectEuler/Python/Problem3.py
#Matthew Ellison
# Created: 01-27-19
#Modified: 07-24-21
#The largest prime factor of 600851475143
#Unless otherwise listed, all of my non-standard imports can be gotten from my pyClasses repository at https://bitbucket.org/Mattrixwv/pyClasses
"""
	Copyright (C) 2021  Matthew Ellison

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Lesser General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""


from Problems.Problem import Problem
import NumberAlgorithms


class Problem3(Problem):
	#Variables
	__goalNumber = 600851475143

	#Functions
	#Constructor
	def __init__(self) -> None:
		super().__init__("What is the largest prime factor of 600851475143?")
		self.factors = []

	#Operational functions
	#Solve the problem
	def solve(self) -> None:
		#If the problem has already been solved do nothing and end the function
		if(self.solved):
			return

		#Start the timer
		self.timer.start()


		#Get the factors of the number
		self.factors = NumberAlgorithms.getFactors(self.__goalNumber)
		#The last element should be the largest factor


		#Stop the timer
		self.timer.stop()

		#Throw a flag to show the problem is solved
		self.solved = True

	#Reset the problem so it can be run again
	def reset(self) -> None:
		super().reset()
		self.factors.clear()

	#Gets
	#Returns the result of solving the problem
	def getResult(self) -> str:
		self.solvedCheck("result")
		return f"The largest prime factor of {self.__goalNumber} is {self.factors[(len(self.factors) - 1)]}"
	#Returns the list of factors of the number
	def getFactors(self) -> list:
		self.solvedCheck("factors")
		return self.factors
	#Returns the largest factor of the number
	def getLargestFactor(self) -> int:
		self.solvedCheck("largest factor")
		return self.factors[(len(self.factors) - 1)]


"""Results:
The largest prime factor of 600851475143 is 6857
It took an average of 2.084 seconds to run this problem through 100 iterations
"""
