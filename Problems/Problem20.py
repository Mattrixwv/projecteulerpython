#ProjectEuler/Python/Problem20.py
#Matthew Ellison
# Created: 03-14-19
#Modified: 10-30-20
#What is the sum of the digits of 100!
#Unless otherwise listed, all of my non-standard imports can be gotten from my pyClasses repository at https://bitbucket.org/Mattrixwv/pyClasses
"""
	Copyright (C) 2020  Matthew Ellison

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Lesser General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""


from Problems.Problem import Problem


class Problem20(Problem):
	#Variables
	__top_num = 100	#The number that you are trying to find the factorial of

	#Functions
	#Constructor
	def __init__(self) -> None:
		super().__init__("What is the sum of the digits of 100!?")
		self.num = 1	#Holds the number 100!
		self.sum = 0	#The sum of the digts of num

	#Operational functions
	#Solve the problem
	def solve(self) -> None:
		#If the problem has already been solved do nothing and end the function
		if(self.solved):
			return

		#Start the timer
		self.timer.start()


		#Run through every number from 1 to 100 and multiply it by the current num to get 100!
		for cnt in range(1, self.__top_num + 1):
			self.num *= cnt

		#Get a string of the number because it is easier to pull appart the individual charaters for the sum
		numString = str(self.num)
		#Run through every character in the string, convert it back to an integer and add it to the running sum
		for char in numString:
			self.sum += int(char)


		#Stop the timer
		self.timer.stop()

		#Throw a flag to show the problem is solved
		self.solved = True
	#Reset the problem so it can be run again
	def reset(self) -> None:
		super().reset()
		self.num = 1
		self.sum = 0

	#Gets
	#Returns the result of solving the problem
	def getResult(self) -> str:
		self.solvedCheck("result")
		return f"100! = {self.num}\nThe sum of the digits is: {self.sum}"
	#Returns the number 100!
	def getNumber(self) -> int:
		self.solvedCheck("number")
		return self.num
	#Returns the sum of the digits of 100!
	def getSum(self) -> int:
		self.solvedCheck("sum of the digits")
		return self.sum


""" Results:
100! = 93326215443944152681699238856266700490715968264381621468592963895217599993229915608941463976156518286253697920827223758251185210916864000000000000000000000000
The sum of the digits is: 648
It took an average of 36.938 microseconds to run this problem through 100 iterations
"""
