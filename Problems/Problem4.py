#ProjectEuler/Python/Problem4.py
#Matthew Ellison
# Created: 01-28-19
#Modified: 07-24-21
#Find the largest palindrome made from the product of two 3-digit numbers
#Unless otherwise listed, all of my non-standard imports can be gotten from my pyClasses repository at https://bitbucket.org/Mattrixwv/pyClasses
"""
	Copyright (C) 2021  Matthew Ellison

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Lesser General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""


from Problems.Problem import Problem


class Problem4(Problem):
	#Variables
	__lowestNum = 100
	__highestNum = 1000

	#Functions
	#Constructor
	def __init__(self) -> None:
		super().__init__("Find the largest palindrome made from the product of two 3-digit numbers")
		self.palindromes = []	#Holds all of the palindromes

	#Operational functions
	#Solve the problem
	def solve(self) -> None:
		#If the problem has already been solved do nothing and end the function
		if(self.solved):
			return

		#Start the timer
		self.timer.start()


		#Loop through every number from __lowestNum to __highestNum twice and multiply every number together
		for firstNum in range(self.__lowestNum, self.__highestNum + 1):
			for secondNum in range(firstNum, self.__highestNum + 1):	#You can start at num1 because 100 * 101 == 101 * 100
				#Get the product
				currentNum = firstNum * secondNum
				#If the number is a palindrome add it to the list of palindromes, otherwise ignore it
				#Using strings makes it easier to determine a palindrome
				if(str(currentNum) == str(currentNum)[::-1]):
					self.palindromes.append(currentNum)
				#If it's not a palindrom ignore it

		#Sort the palindromes so that the last element is the largest
		self.palindromes.sort()


		#Stop the timer
		self.timer.stop()

		#Throw a flag to show the problem is solved
		self.solved = True

	#Reset the problem so it can be run again
	def reset(self) -> None:
		super().reset()
		self.palindromes.clear()

	#Gets
	#Returns the result of solving the problem
	def getResult(self) -> str:
		self.solvedCheck("result")
		return f"The largest palindrome made from the product of two 3-digit numbers is {self.palindromes[len(self.palindromes) - 1]}"
	#Returns the list of all palindromes
	def getPalindromes(self) -> list:
		self.solvedCheck("palindromes")
		return self.palindromes
	#Returns the largest palindrome
	def getLargestPalindrome(self) -> int:
		self.solvedCheck("largest palindrome")
		return self.palindromes[len(self.palindromes) - 1]


"""Results:
The largest palindrome made from the product of two 3-digit numbers is 906609
It took an average of 149.187 milliseconds to run this problem through 100 iterations
"""
