#ProjectEuler/Python/Problem25.py
#Matthew Ellison
# Created: 03-25-19
#Modified: 10-30-20
#What is the index of the first term in the Fibonacci sequence to contain 1000 digits?
#Unless otherwise listed, all of my non-standard imports can be gotten from my pyClasses repository at https://bitbucket.org/Mattrixwv/pyClasses
"""
	Copyright (C) 2020  Matthew Ellison

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Lesser General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""


from Problems.Problem import Problem
import NumberAlgorithms


class Problem25(Problem):
	#Variables
	__numDigits = 1000	#The number of digits to calculate up to

	#Functions
	#Constructor
	def __init__(self) -> None:
		super().__init__("What is the index of the first term in the Fibonacci sequence to contain 1000 digits?")
		self.number = 0	#The current Fibonacci number
		self.index = 2	#The index of the current Fibonacci number just calculated

	#Operational functions
	#Sovle the problem
	def solve(self) -> None:
		#If the problem has already been solved do nothing and end the function
		if(self.solved):
			return

		#Start the timer
		self.timer.start()


		#Move through all Fibonacci numbers until you reach the one with at least __numDigits digits
		while(len(str(self.number)) < self.__numDigits):
			self.index += 1	#Increase the index number. Doing this at the beginning keeps the index correct at the end of the loop
			self.number = NumberAlgorithms.getFib(self.index)	#Calculate the number


		#Stop the timer
		self.timer.stop()

		#Throw a flag to show the problem is solved
		self.solved = True

	#Reset the problem so it can be run again
	def reset(self) -> None:
		super().reset()
		self.number = 0
		self.index = 2

	#Gets
	#Returns the result of solving the problem
	def getResult(self) -> str:
		self.solvedCheck("result")
		return f"The first Fibonacci number with {self.__numDigits} digits is {self.number}\n" \
			f"Its index is {self.index}"
	#Returns the Fibonacci number asked for
	def getNumber(self) -> int:
		self.solvedCheck("fibonacci number")
		return self.number
	#Returns the index of the requested Fibonacci number
	def getIndex(self) -> int:
		self.solvedCheck("index of the fibonacci number")
		return self.index


""" Results:
The first Fibonacci number with 1000 digits is 1070066266382758936764980584457396885083683896632151665013235203375314520604694040621889147582489792657804694888177591957484336466672569959512996030461262748092482186144069433051234774442750273781753087579391666192149259186759553966422837148943113074699503439547001985432609723067290192870526447243726117715821825548491120525013201478612965931381792235559657452039506137551467837543229119602129934048260706175397706847068202895486902666185435124521900369480641357447470911707619766945691070098024393439617474103736912503231365532164773697023167755051595173518460579954919410967778373229665796581646513903488154256310184224190259846088000110186255550245493937113651657039447629584714548523425950428582425306083544435428212611008992863795048006894330309773217834864543113205765659868456288616808718693835297350643986297640660000723562917905207051164077614812491885830945940566688339109350944456576357666151619317753792891661581327159616877487983821820492520348473874384736771934512787029218636250627816
Its index is 4782
It took an average of 3.337 seconds to run this problem through 100 iterations
"""
