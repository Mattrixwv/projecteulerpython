#ProjectEuler/Python/Problem14.py
#Matthew Ellison
# Created: 01-31-19
#Modified: 07-24-21
"""
The following iterative sequence is defined for the set of positive integers:
n → n/2 (n is even)
n → 3n + 1 (n is odd)
Which starting number, under one million, produces the longest chain?
"""
#Unless otherwise listed, all of my non-standard imports can be gotten from my pyClasses repository at https://bitbucket.org/Mattrixwv/pyClasses
"""
	Copyright (C) 2021  Matthew Ellison

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Lesser General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""


from Problems.Problem import Problem


class Problem14(Problem):
	#Variables
	__topNum = 1000000 - 1	#The largest number that you will check against the chain

	#Functions
	#Constructor
	def __init__(self) -> None:
		super().__init__("Which starting number, under one million, produces the longest chain using the itterative sequence?")
		self.maxLength = 0
		self.maxNum = 0

	#Operational functions
	#Solve the problem
	def solve(self) -> None:
		#If the problem has already been solved do nothing and end the function
		if(self.solved):
			return

		#Start the timer
		self.timer.start()


		#Loop through all number <= topNum and check them against the series
		for currentNum in range(1, self.__topNum + 1):
			currentLength = self.checkSeries(currentNum)
			#If the current number has a longer series than the max then the current becomes the max
			if(currentLength > self.maxLength):
				self.maxLength = currentLength
				self.maxNum = currentNum


		#Stop the timer
		self.timer.stop()

		#Throw a flag to show the problem is solved
		self.solved = True

	#This function follows the rules of the sequence and returns its length
	def checkSeries(self, num: int) -> int:
		length = 1	#Start at 1 because you need to count the starting number

		#Follow the series, adding 1 for each step you take
		while(num > 1):
			if((num % 2) == 0):
				num = num / 2
			else:
				num = (3 * num) + 1
			length += 1

		return length

	#Reset the problem so it can be run again
	def reset(self) -> None:
		super().reset()
		self.maxLength = 0
		self.maxNum = 0

	#Gets
	#Returns the result of solving the problem
	def getResult(self) -> str:
		self.solvedCheck("result")
		return f"The number {self.maxNum} produced a chain of {self.maxLength} steps"
	#Returns the length of the requested chain
	def getLength(self) -> int:
		self.solvedCheck("length of the longest chain")
		return self.maxLength
	#Returns the starting number of the requested chain
	def getStartingNumber(self) -> int:
		self.solvedCheck("starting number of the longest chain")
		return self.maxNum


"""Results:
The number 837799 produced a chain of 525 steps
It took an average of 26.591 seconds to run this problem through 100 iterations
"""
