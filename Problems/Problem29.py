#ProjectEuler/Python/Problem29.py
#Matthew Ellison
# Created: 10-10-19
#Modified: 07-24-21
#How many distinct terms are in the sequence generated by a^b for 2 <= a <= 100 and 2 <= b <= 100?
#Unless otherwise listed, all of my non-standard imports can be gotten from my pyClasses repository at https://bitbucket.org/Mattrixwv/pyClasses
"""
	Copyright (C) 2021  Matthew Ellison

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Lesser General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""


from Problems.Problem import Problem


class Problem29(Problem):
	#Variables
	__bottomA = 2	#The lowest possible value for A
	__topA = 100	#The highest possible value for A
	__bottomB = 2	#The lowest possible value for B
	__topB = 100	#The highest possible value for B

	#Functions
	#Constructor
	def __init__(self) -> None:
		super().__init__("How many distinct terms are in the sequence generated by a^b for 2 <= a <= 100 and 2 <= b <= 100?")
		self.unique = []

	#Operational functions
	#Solve the problem
	def solve(self) -> None:
		#If the problem has already been solved do nothing and end the function
		if(self.solved):
			return

		#Start the timer
		self.timer.start()


		#Start with the first A and move towards the top
		for currentA in range(self.__bottomA, self.__topA + 1):
			#Start with the first B and move towards the top
			for currentB in range(self.__bottomB, self.__topB + 1):
				#Get the new number
				currentNum = currentA ** currentB
				#If the new number isn't in the list add it
				if currentNum not in self.unique:
					self.unique.append(currentNum)


		#Stop the timer
		self.timer.stop()

		#Throw a flag to show the problem is solved
		self.solved = True

	#Reset the problem so it can be run again
	def reset(self) -> None:
		super().reset()
		self.unique.clear()

	#Gets
	#Returns the result of solving the problem
	def getResult(self) -> str:
		self.solvedCheck("result")
		return f"The number of unique values generated by a^b for {self.__bottomA} <= a < =  {self.__topA} and {self.__bottomB} <= b <= {self.__topB} is {len(self.unique)}"
	#Returns the lowest possible value for a
	def getBottomA(self) -> int:
		self.solvedCheck("lowest a")
		return self.__bottomA
	#Returns the lowest possible value for a
	def getTopA(self) -> int:
		self.solvedCheck("highest a")
		return self.__topA
	#Returns the lowest possible value for a
	def getBottomB(self) -> int:
		self.solvedCheck("lowest b")
		return self.__bottomB
	#Returns the lowest possible value for a
	def getTopB(self) -> int:
		self.solvedCheck("highest b")
		return self.__topB
	#Returns a list of all unique values for a^b
	def getUnique(self) -> list:
		self.solvedCheck("unique values for a^b")
		return self.unique
	#Returns the number of unique values for a^b
	def getNumUnique(self) -> int:
		self.solvedCheck("number of unique values for a^b")
		return len(self.unique)


""" Results:
The number of unique values generated by a^b for 2 <= a < = 100 and 2 <= b <= 100 is 9183
It took an average of 304.306 milliseconds to run this problem through 100 iterations
"""
