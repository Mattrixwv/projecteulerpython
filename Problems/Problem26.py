#ProjectEuler/Python/Problem26.py
#Matthew Ellison
# Created: 07-29-19
#Modified: 07-24-21
#Find the value of d < 1000 for which 1/d contains the longest recurring cycle in its decimal fraction part.
#Unless otherwise listed, all of my non-standard imports can be gotten from my pyClasses repository at https://bitbucket.org/Mattrixwv/pyClasses
"""
	Copyright (C) 2021  Matthew Ellison

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Lesser General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""


from Problems.Problem import Problem


class Problem26(Problem):
	#Variables
	__topNumber = 999	#The largest denominator to be checked

	#Function
	#Constructor
	def __init__(self) -> None:
		super().__init__("Find the value of d < 1000 for which 1/d contains the longest recurring cycle in its decimal fraction part.")
		self.longestCycle = 0
		self.longestNumber = 0

	#Operational function
	#Solve the problem
	def solve(self) -> None:
		#If the problem has already been solved do nothing and end the function
		if(self.solved):
			return

		#Start the timer
		self.timer.start()


		#Start with 1/2 and find out how long the longest cycle is by checking the remainders
		#Loop through every number from 2-999 and use it for the denominator
		for denominator in range(2, self.__topNumber):
			remainderList = []	#Holds the list of remainders
			endFound = False	#Holds whether we have found an end to the number (either a cycle or a 0 for remainder)
			cycleFound = False	#Holds whether a cycle was detected
			numerator = 1	#The numerator that will be divided
			while(not endFound):
				#Get the remainder after the division
				remainder = numerator % denominator
				#Check if the remainder is 0
				#If it is set the flag
				if(remainder == 0):
					endFound = True
				#Check if the remainder is in the list
				#If it is in the list, set the appropriate flags
				elif remainder in remainderList:
					endFound = True
					cycleFound = True
				#Else add it to the list
				else:
					remainderList.append(remainder)

				#Multiply the remainder by 10 to continue finding the next remainder
				numerator = remainder * 10
			#If a cycle was found check the size of the list against the largest cycle
			if(cycleFound):
				#If it is larger than the largest, set it as the new largest
				if(len(remainderList) > self.longestCycle):
					self.longestCycle = len(remainderList)
					self.longestNumber = denominator


		#Stop the timer
		self.timer.stop()

		#Throw a flag to show the problem is solved
		self.solved = True

	#Reset the problem so it can be run again
	def reset(self) -> None:
		super().reset()
		self.longestCycle = 0
		self.longestNumber = 0

	#Gets
	#Returns the result of solving the problem
	def getResult(self) -> str:
		self.solvedCheck("result")
		return f"The longest cycle is {self.longestCycle} digits long\n" \
			f"It is started with the number {self.longestNumber}"
	#Returns the length of the longest cycle
	def getLongestCycle(self) -> int:
		self.solvedCheck("length of the longest cycle")
		return self.longestCycle
	#Returns the denominator that starts the longest cycle
	def getLongestNumber(self) -> int:
		self.solvedCheck("denominator that starts the longest cycle")
		return self.longestNumber


""" Results:
The longest cycle is 982 digits long
It is started with the number 983
It took an average of 139.229 milliseconds to run this problem through 100 iterations
"""
