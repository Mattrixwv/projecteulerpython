#ProjectEuler/Python/Problem24.py
#Matthew Ellison
# Created: 03-24-19
#Modified: 07-24-21
#What is the millionth lexicographic permutation of the digits 0, 1, 2, 3, 4, 5, 6, 7, 8 and 9?
#Unless otherwise listed, all of my non-standard imports can be gotten from my pyClasses repository at https://bitbucket.org/Mattrixwv/pyClasses
"""
	Copyright (C) 2021  Matthew Ellison

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Lesser General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""


from Problems.Problem import Problem
import StringAlgorithms


class Problem24(Problem):
	#Variables
	__neededPerm = 1000000	#The number of the permutation that you need
	__nums = "0123456789"	#All of the characters that we need to get the permutations of

	#Functions
	#Constructor
	def __init__(self) -> None:
		super().__init__("What is the millionth lexicographic permutation of the digits 0, 1, 2, 3, 4, 5, 6, 7, 8 and 9?")
		self.permutations = []

	#Operational functions
	#Solve the problem
	def solve(self) -> None:
		#If the problem has already been solved do nothing and end the function
		if(self.solved):
			return

		#Start the timer
		self.timer.start()


		#Get all permutations of the string
		self.permutations = StringAlgorithms.getPermutations(self.__nums)


		#Stop the timer
		self.timer.stop()

		#Throw a flag to show the problem is solved
		self.solved = True

	#Reset the problem so it can be run again
	def reset(self) -> None:
		super().reset()
		self.permutations.clear()

	#Gets
	#Returns the result of solving the problem
	def getResult(self) -> str:
		self.solvedCheck("result")
		return f"The 1 millionth permutation is {self.permutations[self.__neededPerm - 1]}"
	#Returns a list with all of the permutations
	def getPermutationsList(self) -> list:
		self.solvedCheck("permutations")
		return self.permutations
	#Returns the requested permutation
	def getPermutation(self) -> str:
		self.solvedCheck("1,000,000th permutation")
		return self.permutations[self.__neededPerm - 1]


""" Results:
The 1 millionth permutation is 2783915460
It took an average of 7.147 seconds to run this problem through 100 iterations
"""
