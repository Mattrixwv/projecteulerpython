#Project Euler/Python/Problem9.py
#Matthew Ellison
# Created: 01-29-19
#Modified: 07-24-21
#There exists exactly one Pythagorean triplet for which a + b + c = 1000. Find the product abc.
#Unless otherwise listed, all of my non-standard imports can be gotten from my pyClasses repository at https://bitbucket.org/Mattrixwv/pyClasses
"""
	Copyright (C) 2021  Matthew Ellison

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Lesser General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""


from Problems.Problem import Problem
import math


class Problem9(Problem):
	#Functions
	#Constructor
	def __init__(self) -> None:
		super().__init__("There exists exactly one Pythagorean triplet for which a + b + c = 1000. Find the product abc.")
		self.a = 1
		self.b = 0
		self.c = 0
		self.found = False

	#Operational functions
	#Solve the problem
	def solve(self) -> None:
		#If the problem has already been solved do nothing and end the function
		if(self.solved):
			return

		#Start the timer
		self.timer.start()


		#Start with the lowest possible a , 1, and search for the b and c to complete the triplet
		while((self.a <= (1000 / 3)) and (not self.found)):
			#Setup b and c
			self.b = self.a + 1	#b must be > a to be a triplet
			self.c = math.hypot(self.a, self.b)	#C is the hyp
			#Loop through possible b's and calculate c's until you find the numbers or the sum gets too large
			while((self.a + self.b + self.c) < 1000):
				self.b += 1
				self.c = math.hypot(self.a, self.b)
				#If c is an integer make it one
				if((self.c % 1) == 0):
					self.c = int(round(self.c))
			#Check if the correct sides were found
			if((self.a + self.b + self.c) == 1000):
				self.found = True
			#Otherwise increment a to the next possible number
			else:
				self.a += 1


		#Stop the timer
		self.timer.stop()

		#Throw a flag to show the problem is solved
		self.solved = True

	#Reset the problem so it can be run again
	def reset(self) -> None:
		super().reset()
		self.a = 1
		self.b = 0
		self.c = 0
		self.found = False

	#Gets
	#Returns the result of solving the problem
	def getResult(self) -> str:
		self.solvedCheck("result")
		return f"The Pythagorean triplet where a + b + c = 1000 is {self.a} {self.b} {int(self.c)}\n" \
			f"The product of those numbers is {int(self.a * self.b * self.c)}"
	#Returns the length of the first side
	def getSideA(self) -> int:
		self.solvedCheck("first side")
		return self.a
	#Returns the length of the second side
	def getSideB(self) -> int:
		self.solvedCheck("second side")
		return self.b
	#Returns the length of the hyp
	def getSideC(self) -> float:
		self.solvedCheck("third side")
		return self.c
	#Returns the product of the 3 sides
	def getProduct(self) -> int:
		self.solvedCheck("product of all three sides")
		return int(self.a * self.b * self.c)


"""Results:
The Pythagorean triplet where a + b + c = 1000 is 200 375 425
The product of those numbers is 31875000
It took an average of 36.729 milliseconds to run this problem through 100 iterations
"""
