#ProjectEuler/ProjectEulerPython/Problems/Problem34.py
#Matthew Ellison
# Created: 06-01-21
#Modified: 07-21-21
#Find the sum of all numbers which are equal to the sum of the factorial of their digits
#Unless otherwise listed, all of my non-standard imports can be gotten from my pyClasses repository at https://bitbucket.org/Mattrixwv/pyClasses
"""
	Copyright (C) 2021  Matthew Ellison

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Lesser General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""


from Problems.Problem import Problem
import NumberAlgorithms


class Problem34(Problem):
	#Variables
	__max_num = 1499999

	#Functions
	#Constructor
	def __init__(self) -> None:
		super().__init__("Find the sum of all numbers which are equal to the sum of the factorial of their digits")
		self.totalSum = 0
		self.factorials = []
		for _ in range(0, 10):
			self.factorials.append(0)

	#Operational functions
	#Solve the problem
	def solve(self) -> None:
		#If the problem has already been solved do nothing and end the function
		if(self.solved):
			return

		#Start the timer
		self.timer.start()


		#Pre-compute the possible factorials from 0! to 9!
		for cnt in range(0, 10):
			self.factorials[cnt] = NumberAlgorithms.factorial(cnt)
		#Run through all possible numbers from 3-MAX_NUM and see if they equal the sum of their digit's factorials
		for cnt in range(3, self.__max_num + 1):
			numString = str(cnt)
			currentSum = 0
			for char in numString:
				currentSum += self.factorials[int(char)]
			#If the number is equal to the sum add the sum to the running sum
			if(currentSum == cnt):
				self.totalSum += currentSum


		#Stop the timer
		self.timer.stop()

		#Throw a flag to show the porblem is solved
		self.solved = True

	#Reset the problem so it can be run again
	def reset(self) -> None:
		super().reset()
		self.totalSum = 0
		self.factorials = []
		for _ in range(0, 10):
			self.factorials.append(0)

	#Gets
	#Returns the result of solving the problem
	def getResult(self) -> str:
		self.solvedCheck("result")
		return f"The sum of all numbers that are the sum of their digit's factorials is {self.totalSum}"
	#Returns the list of factorials from 0-9
	def getFactorials(self) -> list:
		self.solvedCheck("list of factorials")
		return self.factorials
	#Returns the sum of all numbers equal to the sum of their digit's factorials
	def getSum(self) -> int:
		self.solvedCheck("sum")
		return self.totalSum


""" Results:
The sum of all numbers that are the sum of their digit's factorials is 40730
It took an average of 2.337 seconds to run this problem through 100 iterations
"""
