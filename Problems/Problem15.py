#ProjectEuler/Python/Problem15.py
#Matthew Ellison
# Created: 01-31-19
#Modified: 07-24-21
#How many routes from the top left corner to the bottom right corner are there through a 20×20 grid if you can only move right and down?
#Unless otherwise listed, all of my non-standard imports can be gotten from my pyClasses repository at https://bitbucket.org/Mattrixwv/pyClasses
"""
	Copyright (C) 2021  Matthew Ellison

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Lesser General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""


from Problems.Problem import Problem


class Problem15(Problem):
	#Variables
	__gridWidth = 20	#The height of the grid
	__gridHeight = 20	#The width of the grid

	#Functions
	#Constructor
	def __init__(self) -> None:
		super().__init__("How many routes from the top left corner to the bottom right corner are there through a 20x20 grid if you can only move right and down?")
		self.numOfRoutes = 0

	#Operational functions
	#Solve the problem
	def solve(self) -> None:
		#If the problem has already been solved do nothing and end the function
		if(self.solved):
			return

		#Start the timer
		self.timer.start()


		#Start the recursion at the right location and catch what is returned
		self.numOfRoutes = self.movement(0, 0)


		#Stop the timer
		self.timer.stop()

		#Throw a flag to show the problem is solved
		self.solved = True

	#This function acts as a handler for moving the position on the grid and counting the distance
	#It moves right first, then down
	def movement(self, currentX: int, currentY: int) -> int:
		#Return 1 if you are at the finish location
		if((currentX == self.__gridWidth) and (currentY == self.__gridHeight)):
			return 1

		numberMoves = 0
		#Otherwise move one right if you can and recurse
		if(currentX < self.__gridWidth):
			numberMoves = self.movement(currentX + 1, currentY)

		#Then move one down and recurse
		if(currentY < self.__gridHeight):
			numberMoves += self.movement(currentX, currentY + 1)

		return numberMoves

	#Reset the problem so it can be run again
	def reset(self) -> None:
		super().reset()
		self.numOfRoutes = 0

	#Gets
	#Returns the result of solving the problem
	def getResult(self) -> str:
		self.solvedCheck("result")
		return f"The number of paths from 1 corner of a {self.__gridWidth} x {self.__gridHeight} grid to the opposite corner is {self.numOfRoutes}"
	#Returns the number of routes found
	def getNumberOfRoutes(self) -> int:
		self.solvedCheck("number of routes")
		return self.numOfRoutes


"""Results:
The number of paths from 1 corner of a 20 x 20 grid to the opposite corner is 137846528820
It took 32.903 hours to solve this algorithm
"""
