#ProjectEuler/Python/Problem19.py
#Matthew Ellison
# Created: 03-13-19
#Modified: 07-24-21
#How many Sundays fell on the first of the month during the twentieth century (1 Jan 1901 to 31 Dec 2000)?
"""
You are given the following information, but you may prefer to do some research for yourself.
1 Jan 1900 was a Monday.
Thirty days has September,
April, June and November.
All the rest have thirty-one,
Saving February alone,
Which has twenty-eight, rain or shine.
And on leap years, twenty-nine.
A leap year occurs on any year evenly divisible by 4, but not on a century unless it is divisible by 400.
"""
#Unless otherwise listed, all of my non-standard imports can be gotten from my pyClasses repository at https://bitbucket.org/Mattrixwv/pyClasses
"""
	Copyright (C) 2021  Matthew Ellison

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Lesser General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""


from Problems.Problem import Problem


class DAYS:
	SUNDAY = 0
	MONDAY = 1
	TUESDAY = 2
	WEDNESDAY = 3
	THURSDAY = 4
	FRIDAY = 5
	SATURDAY = 6
	NUMBER_OF_DAYS = 7
	ERROR = 8

class Problem19(Problem):
	#Variables
	__startYear = 1901	#The year we start counting sundays
	__endYear = 2000	#The year we stop counting sundays

	#Functions
	#Constructor
	def __init__(self) -> None:
		super().__init__("How many Sundays fell on the first of the month during the twentieth century (1 Jan 1901 to 31 Dec 2000)?")
		self.totalSundays = 0	#Keep track of the number of sundays

	#Operational functions
	#Solve the problem
	def solve(self) -> None:
		#If the problem has already been solved do nothing and end the function
		if(self.solved):
			return

		#Start the timer
		self.timer.start()


		#Run for all years from start to end
		for year in range(self.__startYear, self.__endYear + 1):
			#Run for all months in the year
			for month in range(1, 13):
				day = self.getDay(month, 1, year)
				if(day == DAYS.ERROR):
					self.result = "There was an error with the day"
					return
				elif(day == DAYS.SUNDAY):
					self.totalSundays += 1


		#Stop the timer
		self.timer.stop()

		#Throw a flag to show the problem is solved
		self.solved = True

	#Return the day of the week that the date you pass into it is on
	def getDay(self, month: int, day: int, year: int) -> DAYS:
		#Make sure the numebrs are within propper bounds
		if((month < 1) or (month > 12) or (day < 1) or (day > 31) or (year < 1)):
			return DAYS.ERROR

		numDays = 0	#The number of days between 01-01-0001 and the date given
		currentYear = 1
		currentMonth = 1
		currentDay = DAYS.SATURDAY
		day -= 1

		#Add the correct number of days for every year
		while(currentYear < year):
			if(self.isLeapYear(currentYear)):
				numDays += 366
			else:
				numDays += 365
			currentYear += 1
		#Add the correct number of days for eveyr month
		while(currentMonth < month):
			#February
			if(currentMonth == 2):
				if(self.isLeapYear(currentYear)):
					numDays += 29
				else:
					numDays += 28
			elif((currentMonth == 1) or (currentMonth == 3) or (currentMonth == 5) or (currentMonth == 7) or (currentMonth == 8) or (currentMonth == 10) or (currentMonth == 12)):
				numDays += 31
			#For 30 day months
			else:
				numDays += 30
			currentMonth += 1
		#Account for the weird year of 1752
		if(year > 1752):
			numDays -= 11
		elif(year == 1752):
			if(month > 9):
				numDays -= 11
			elif(month == 9):
				if(day >= 14):
					numDays -= 11
				#Days 3-13 were skipped that year
				elif((day > 2) and (day < 14)):
					return DAYS.ERROR
		#Add the correct number of days for every day
		numDays += day

		currentDay += numDays
		currentDay = currentDay % DAYS.NUMBER_OF_DAYS
		if(currentDay == DAYS.SUNDAY):
			return DAYS.SUNDAY
		elif(currentDay == DAYS.MONDAY):
			return DAYS.MONDAY
		elif(currentDay == DAYS.TUESDAY):
			return DAYS.TUESDAY
		elif(currentDay == DAYS.WEDNESDAY):
			return DAYS.WEDNESDAY
		elif(currentDay == DAYS.THURSDAY):
			return DAYS.THURSDAY
		elif(currentDay == DAYS.FRIDAY):
			return DAYS.FRIDAY
		elif(currentDay == DAYS.SATURDAY):
			return DAYS.SATURDAY
		else:
			return DAYS.ERROR

	#Returns true if the year passed to it is a leap year
	def isLeapYear(self, year: int) -> bool:
		if(year < 1):
			return False
		elif((year % 100) == 0):
			#This rule only applies at and after 1800
			if(year <= 1700):
				return True
			elif((year % 400) == 0):
				return True
		elif((year % 4) == 0):
			return True
		return False

	#Reset the problem so it can be run again
	def reset(self) -> None:
		super().reset()
		self.totalSundays = 0

	#Gets
	#Returns the result of solving the problem
	def getResult(self) -> str:
		self.solvedCheck("result")
		return f"There are {self.totalSundays} Sundays that landed on the first of the month from {self.__startYear} to {self.__endYear}"
	#Returns the total sundays that were asked for
	def getTotalSundays(self) -> int:
		self.solvedCheck("total number of sundays")
		return self.totalSundays


""" Results:
There are 171 Sundays that landed on the first of the month from 1901 to 2000
It took an average of 386.694 milliseconds to run this problem through 100 iterations
"""
