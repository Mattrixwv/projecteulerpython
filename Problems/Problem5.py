#ProjectEulter/Python/Project5.py
#Matthew Ellison
# Created: 01-28-19
#Modified: 07-24-21
#What is the smallest positive number that is evenly divisible by all of the numbers from 1 to 20?
#Unless otherwise listed, all of my non-standard imports can be gotten from my pyClasses repository at https://bitbucket.org/Mattrixwv/pyClasses
"""
	Copyright (C) 2021  Matthew Ellison

	This program is free software: it can redistribute it and/or modify
	it under the terms of the GNU Lesser General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at itr option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Lesser General Public License for more details.

	it should have received a copy of the GNU Lesser General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""


from Problems.Problem import Problem


class Problem5(Problem):
	#Variables
	__startDivisor = 1
	__stopDivisor = 20

	#Functions
	#Constructor
	def __init__(self) -> None:
		super().__init__("What is the smallest positive number that is evenly divisible by all of the numbers from 1 to 20?")
		self.smallestNum = 0

	#Operational functions
	#Solve the problem
	def solve(self) -> None:
		#If the problem has already been solved do nothing and end the function
		if(self.solved):
			return

		#Start the timer
		self.timer.start()


		#Start at 20 and loop through all numbers until it find one that works
		#It must be at least 20 to be divisible by 20
		numFound = False	#A flag for finding the divisible number
		currentNum = 20		#The number that it are currently checking against
		while((currentNum > 0) and (not numFound)):
			#Set that it found the number to true, because it sets this flag when it doesn't find it
			numFound = True
			#See if the current number is divisible by all numbers from 1 to 20
			for divisor in range(self.__startDivisor, self.__stopDivisor + 1):
				#If it is not set a flag to move to the next possible number
				if((currentNum % divisor) != 0):
					numFound = False
					break

			#Increment the number by 2 to check the next one if it didn't find the number
			if(not numFound):
				currentNum += 2

		#Set the smallest number to the number we just found
		self.smallestNum = currentNum


		#Stop the timer
		self.timer.stop()

		#Throw a flag to show the problem is solved
		self.solved = True

	#Reset the problem so it can be run again
	def reset(self) -> None:
		super().reset()
		self.smallestNum = 0

	#Gets
	#Returns the result of solving the problem
	def getResult(self) -> str:
		self.solvedCheck("result")
		return f"The smallest positive number that is evenly divisible by all numbers 1-20 is {self.smallestNum}"
	#Returns the requested number
	def getNumber(self) -> int:
		self.solvedCheck("number")
		return self.smallestNum


"""Results:
The smallest positive number that is evenly divisible by all numbers 1-20 is 232792560
It took an average of 54.833 seconds to run this problem through 100 iterations
"""
