#Project Eulter/Python/Problem7.py
#Matthew Ellison
# Created: 01-29-19
#Modified: 07-24-21
#What is the 10001st prime number?
#Unless otherwise listed, all of my non-standard imports can be gotten from my pyClasses repository at https://bitbucket.org/Mattrixwv/pyClasses
"""
	Copyright (C) 2021  Matthew Ellison

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Lesser General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""


from Problems.Problem import Problem
import NumberAlgorithms


class Problem7(Problem):
	#Variables
	__numPrimes = 10001		#The number of the prime number desired

	#Functions
	#Constructor
	def __init__(self) -> None:
		super().__init__("What is the 10001st prime number?")
		self.primes = []

	#Operational functions
	#Solve the problem
	def solve(self) -> None:
		#If the problem has already been solved do nothing and end the function
		if(self.solved):
			return

		#Start the timer
		self.timer.start()


		#Get the correct number of primes
		self.primes = NumberAlgorithms.getNumPrimes(self.__numPrimes)


		#Stop the timer
		self.timer.stop()

		#Throw a flag to show the problem is solved
		self.solved = True

	#Reset the problem so it can be run again
	def reset(self) -> None:
		super().reset()
		self.primes.clear()

	#Gets
	#Returns the result of solving the problem
	def getResult(self) -> str:
		self.solvedCheck("result")
		return f"The {self.__numPrimes}st prime number is {self.primes[self.__numPrimes - 1]}"
	#Returns the requested prime number
	def getPrime(self) -> int:
		self.solvedCheck("prime")
		return self.primes[len(self.primes) - 1]


"""Results:
The 10001st prime number is 104743
It took an average of 109.101 milliseconds to run this problem through 100 iterations
"""
