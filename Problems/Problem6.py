#ProjectEuler/Python/Problem6.py
#Matthew Ellison
# Created: 01-28-19
#Modified: 07-24-21
#Find the difference between the sum of the squares of the first one hundred natural numbers and the square of the sum
#Unless otherwise listed, all of my non-standard imports can be gotten from my pyClasses repository at https://bitbucket.org/Mattrixwv/pyClasses
"""
	Copyright (C) 2021  Matthew Ellison

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Lesser General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""


from Problems.Problem import Problem


class Problem6(Problem):
	#Variables
	__startNum = 1
	__endNum = 100

	#Functions
	#Constructor
	def __init__(self) -> None:
		super().__init__("Find the difference between the sum of the squares of the first one hundred natural numbers and the square of the sum.")
		self.sumOfSquares = 0	#Holds the sum of the squares of all the numbers
		self.squareOfSum = 0	#Holds the square of the sum of all the numbers

	#Operational functions
	#Solve the problem
	def solve(self) -> None:
		#If the problem has already been solved do nothing and end the function
		if(self.solved):
			return

		#Start the timer
		self.timer.start()


		#Run through all numbers from 1-100 and add them to the approriate sums
		for num in range(self.__startNum, self.__endNum + 1):
			self.sumOfSquares += (num * num)	#Get the sum of the squares of the first 100 natural numbers
			self.squareOfSum += num		#Get the sum of the first 100 natural numbers so you can square it later
		#Square the normal sum
		self.squareOfSum *= self.squareOfSum


		#Stop the timer
		self.timer.stop()

		#Throw a flag to show the problem is solved
		self.solved = True

	#Reset the problem so it can be run again
	def reset(self) -> None:
		super().reset()
		self.squareOfSum = 0
		self.sumOfSquares = 0

	#Gets
	#Returns the result of solving the problem
	def getResult(self) -> str:
		self.solvedCheck("result")
		return f"The difference between the sum of the squares and the square of the sum of the numbers 1-100 is {abs(self.sumOfSquares - self.squareOfSum)}"
	#Returns the sum of all the squares
	def getSumOfSquares(self) -> int:
		self.solvedCheck("sum of the squares")
		return self.sumOfSquares
	#Return the sqare of all of the sums
	def getSquareOfSum(self) -> int:
		self.solvedCheck("square of the sums")
		return self.squareOfSum
	#Returns the requested difference
	def getDifference(self) -> int:
		self.solvedCheck("difference between the two numbers")
		return abs(self.sumOfSquares - self.squareOfSum)


"""Results:
The difference between the sum of the squares and the square of the sum of the numbers 1-100 is 25164150
It took an average of 14.266 microseconds to run this problem through 100 iterations
"""
