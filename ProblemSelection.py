#ProjectEulerPython/ProblemSelection.py
#Matthew Ellison
# Created: 07-19-20
#Modified: 10-20-21
#This is the driver function for the Java version of the ProjectEuler project
"""
Copyright (C) 2021  Matthew Ellison

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Lesser General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""


from Problems.Problem import Problem
from Problems.Problem1 import Problem1
from Problems.Problem2 import Problem2
from Problems.Problem3 import Problem3
from Problems.Problem4 import Problem4
from Problems.Problem5 import Problem5
from Problems.Problem6 import Problem6
from Problems.Problem7 import Problem7
from Problems.Problem8 import Problem8
from Problems.Problem9 import Problem9
from Problems.Problem10 import Problem10
from Problems.Problem11 import Problem11
from Problems.Problem12 import Problem12
from Problems.Problem13 import Problem13
from Problems.Problem14 import Problem14
from Problems.Problem15 import Problem15
from Problems.Problem16 import Problem16
from Problems.Problem17 import Problem17
from Problems.Problem18 import Problem18
from Problems.Problem19 import Problem19
from Problems.Problem20 import Problem20
from Problems.Problem21 import Problem21
from Problems.Problem22 import Problem22
from Problems.Problem23 import Problem23
from Problems.Problem24 import Problem24
from Problems.Problem25 import Problem25
from Problems.Problem26 import Problem26
from Problems.Problem27 import Problem27
from Problems.Problem28 import Problem28
from Problems.Problem29 import Problem29
from Problems.Problem30 import Problem30
from Problems.Problem31 import Problem31
from Problems.Problem32 import Problem32
from Problems.Problem33 import Problem33
from Problems.Problem34 import Problem34
from Problems.Problem35 import Problem35
from Problems.Problem36 import Problem36
from Problems.Problem37 import Problem37
from Problems.Problem38 import Problem38
from Problems.Problem67 import Problem67


class ProblemSelection:
	#Holds the valid problem numbers
	problemNumbers = [ 0,  1,  2,  3,  4,  5,  6,  7,  8,  9, 10,
						  11, 12, 13, 14, 15, 16, 17, 18, 19, 20,
						  21, 22, 23, 24, 25, 26, 27, 28, 29, 30,
						  31, 32, 33, 34, 35, 36, 37, 38, 67]

	#Returns the problem corresponding to the given problem number
	@staticmethod
	def getProblem(problemNumber: int) -> Problem:
		if(problemNumber == 1):
			return Problem1()
		elif(problemNumber == 2):
			return Problem2()
		elif(problemNumber == 3):
			return Problem3()
		elif(problemNumber == 4):
			return Problem4()
		elif(problemNumber == 5):
			return Problem5()
		elif(problemNumber == 6):
			return Problem6()
		elif(problemNumber == 7):
			return Problem7()
		elif(problemNumber == 8):
			return Problem8()
		elif(problemNumber == 9):
			return Problem9()
		elif(problemNumber == 10):
			return Problem10()
		elif(problemNumber == 11):
			return Problem11()
		elif(problemNumber == 12):
			return Problem12()
		elif(problemNumber == 13):
			return Problem13()
		elif(problemNumber == 14):
			return Problem14()
		elif(problemNumber == 15):
			return Problem15()
		elif(problemNumber == 16):
			return Problem16()
		elif(problemNumber == 17):
			return Problem17()
		elif(problemNumber == 18):
			return Problem18()
		elif(problemNumber == 19):
			return Problem19()
		elif(problemNumber == 20):
			return Problem20()
		elif(problemNumber == 21):
			return Problem21()
		elif(problemNumber == 22):
			return Problem22()
		elif(problemNumber == 23):
			return Problem23()
		elif(problemNumber == 24):
			return Problem24()
		elif(problemNumber == 25):
			return Problem25()
		elif(problemNumber == 26):
			return Problem26()
		elif(problemNumber == 27):
			return Problem27()
		elif(problemNumber == 28):
			return Problem28()
		elif(problemNumber == 29):
			return Problem29()
		elif(problemNumber == 30):
			return Problem30()
		elif(problemNumber == 31):
			return Problem31()
		elif(problemNumber == 32):
			return Problem32()
		elif(problemNumber == 33):
			return Problem33()
		elif(problemNumber == 34):
			return Problem34()
		elif(problemNumber == 35):
			return Problem35()
		elif(problemNumber == 36):
			return Problem36()
		elif(problemNumber == 37):
			return Problem37()
		elif(problemNumber == 38):
			return Problem38()
		elif(problemNumber == 67):
			return Problem67()

	#Print the description of a problem
	@staticmethod
	def printDescription(problemNumber: int):
		#Get the problem
		problem = ProblemSelection.getProblem(problemNumber)
		#Print the problem's description
		print(problem.getDescription())

	#Solve a problem
	@staticmethod
	def solveProblem(problemNumber: int):
		#Get the problem
		problem = ProblemSelection.getProblem(problemNumber)
		#Print the problem description
		print(problem.getDescription())
		#Solve the problem
		problem.solve()
		#Print the results
		print(problem.getResult())
		print("It took " + problem.getTime() + " to solve this algorithm\n\n")

	#Get a valid problem number from a user
	@staticmethod
	def getProblemNumber() -> int:
		problemNumber = int(input("Enter a problem number: "))
		while not (problemNumber in ProblemSelection.problemNumbers):
			print("That is an invalid problem number!")
			problemNumber = int(input("Enter a problem number: "))
		return problemNumber

	#List all valid problem numbers
	@staticmethod
	def listProblems():
		problemList = str(ProblemSelection.problemNumbers[1])
		for problemNumber in range(2, len(ProblemSelection.problemNumbers)):
			problemList += ", " + str(ProblemSelection.problemNumbers[problemNumber])
		print(problemList + "\n")
